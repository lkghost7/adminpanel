﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using AutoIt;

namespace AdminPanel.Classes
{
    public class PowerShellAdmin
    {
        public string RunPowerShellScript(string scriptPath)
        {
            using (PowerShell PowerShellInstance = PowerShell.Create())
            {
                // Читаем содержимое PowerShell-скрипта из файла
                string scriptContent = System.IO.File.ReadAllText(scriptPath);

                // Добавляем скрипт к экземпляру PowerShell
                PowerShellInstance.AddScript(scriptContent);

                // Выполняем скрипт
                Collection<PSObject> PSOutput = PowerShellInstance.Invoke();

                // Проверяем наличие ошибок
                if (PowerShellInstance.HadErrors)
                {
                    foreach (ErrorRecord error in PowerShellInstance.Streams.Error)
                    {
                        Console.WriteLine($"PowerShell Error: {error}");
                    }

                    return null;
                }

                // Извлекаем возвращенное значение
                string returnedValue = PSOutput.Count > 0 ? PSOutput[0].ToString() : null;

                return returnedValue;
            }
        }
        
        public void CredentialRunPowerShell(string pass, string salonName, string scriptPath, string username)
        {
            PowerShell ps = PowerShell.Create();
            
            string credentialPath = @"C:\temp\AdminScripts\Credentials.ps1";
            ps.AddScript(File.ReadAllText(credentialPath));
            ps.AddParameter("Userpc", salonName);
            ps.AddParameter("Password", pass);
            ps.AddParameter("ScriptPath", scriptPath);
            ps.AddParameter("Username", username);

            ps.Invoke<string>();
        }
        
        public void InDomainPowerShell(string pass, string PcName, string loginName)
        {
            PowerShell ps = PowerShell.Create();
            
            string scriptPath = @"C:\temp\AdminScripts\InDomain.ps1";
            ps.AddScript(File.ReadAllText(scriptPath));
            ps.AddParameter("PcName", PcName);
            ps.AddParameter("Password", pass);
            ps.AddParameter("LoginName", loginName);

            ps.Invoke<string>();
        }
        
        public void ChangeNAmePowerShell(string pass, string PcName, string loginName)
        {
            PowerShell ps = PowerShell.Create();
            
            string scriptPath = @"C:\temp\AdminScripts\ChangeName.ps1";
            ps.AddScript(File.ReadAllText(scriptPath));
            ps.AddParameter("PcName", PcName);
            ps.AddParameter("Password", pass);
            ps.AddParameter("LoginName", loginName);

            ps.Invoke<string>();
        }

        public void EnablePohForce()
        {
            PowerShell ps = PowerShell.Create();
            ps.AddScript("Set-ExecutionPolicy -Scope CurrentUser RemoteSigned");
            ps.Invoke();
        }
        
        private void PressAddAdminBTN3(object sender, EventArgs e)
        {
            string userpc = Environment.UserName;
            MessageBox.Show(userpc);
            // inputSalonName.Text = userpc;
        }
        
        public string[] GetAllIPv4Addresses()
        {
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            networkInterfaces = networkInterfaces
                .Where(nic => nic.OperationalStatus == OperationalStatus.Up && !nic.Description.ToLower().Contains("virtual"))
                .ToArray();

            string[] ipv4Addresses = networkInterfaces
                .SelectMany(nic => nic.GetIPProperties().UnicastAddresses)
                .Where(address => address.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                .Select(address => address.Address.ToString())
                .ToArray();

            return ipv4Addresses.Length > 0 ? ipv4Addresses : new string[] { "N/A" };
        }


    }
}