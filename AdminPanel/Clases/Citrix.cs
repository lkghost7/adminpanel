﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using AutoIt;

namespace AdminPanel.Classes
{
    public class Citrix
    {
        public Action CitrixInstallComplete;

        public void CloseCitrixProcess(string aa)
        {
            // Process[] prc = Process.GetProcesses();
            //
            // foreach (Process process in prc)
            // {
            //     MessageBox.Show(process.ProcessName);
            // }

            // // string processName = "Receiver.exe"; // Замените на фактическое имя процесса
            // string processName = "Блокнот"; // Замените на фактическое имя процесса
            //
            // Process[] processes = Process.GetProcessesByName(processName);
            //
            // if (processes.Length > 0)
            // {
            //     foreach (Process process in processes)
            //     {
            //         try
            //         {
            //             process.Kill();
            //         }
            //         catch (Exception ex)
            //         {
            //             MessageBox.Show("Ошибка при закрытии процесса: " + ex.Message, "Ошибка");
            //         }
            //     }
            // }
            // else
            // {
            //     MessageBox.Show("Процесс Citrix не найден" +  processName, "Ошибка");
            // }
        }
        
        public void CloseCitrixProcess2(string aa)
        {
            string processName = "notepad"; // Замените на имя процесса, который вы ищете

            if (AutoItX.WinExists(processName) != 0)
            {
                MessageBox.Show($"Процесс {processName} найден.");
                // AutoItX.WinKill(processName);
     
                // Здесь вы можете выполнить необходимые действия с найденным процессом
            }
            else
            {
                MessageBox.Show($"Процесс {processName} не найден.");
            }
        }
        
        public void CloseCitrixProcess3(string aa)
        {
            string pathToExe = @"C:\Program Files (x86)\Citrix\ICA Client\SelfServicePlugin\SelfService.exe";
            Process.Start(pathToExe);
            Thread.Sleep(7000);
            
            string windowTitle = "Citrix Workspace";
            AutoItX.WinActivate("[TITLE:" + windowTitle + "]");
            Thread.Sleep(170);
            AutoItX.Send("xd1.nasvyazi.local");
            Thread.Sleep(700);
            AutoItX.Send("{ENTER}");
        }
        
        public void CitrixSetup(string target)
        {
            string pathToExe = @"C:\Program Files (x86)\Citrix\ICA Client\SelfServicePlugin\SelfService.exe";

            if (System.IO.File.Exists(pathToExe))
            {
                Process.Start(pathToExe);
            }
            else
            {
                MessageBox.Show("Установите Citrix", "Ошибка пути");
                return;
            }
            
            string windowTitle = "Citrix Workspace";
            AutoItX.WinActivate("[TITLE:" + windowTitle + "]");
            Thread.Sleep(8000);
            AutoItX.WinSetState("[TITLE:" + windowTitle + "]", "", AutoItX.SW_MAXIMIZE);
            Thread.Sleep(1000);
            
            AutoItX.Send("xd1.nasvyazi.local");
            Thread.Sleep(300);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(4000);
            AutoItX.Send(target);
            Thread.Sleep(300);
            AutoItX.Send("{TAB}");
            Thread.Sleep(300);
            AutoItX.Send("q123456");
            Thread.Sleep(300);
            AutoItX.Send("{TAB}");
            AutoItX.Send("{TAB}");
            Thread.Sleep(250);
            AutoItX.Send("{SPACE}");
            Thread.Sleep(300);
            AutoItX.Send("{ENTER}");
            AutoItX.WinSetState("[TITLE:" + windowTitle + "]", "", AutoItX.SW_MINIMIZE);

            // CitrixInstallComplete.Invoke();
        }
    }
}