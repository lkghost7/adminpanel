﻿using System.Drawing;
using System.Windows.Forms;

namespace AdminPanel.Classes
{
    public class ButtonsInstall
    {
        // for program
        public Button GetCitrixInstallButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Поставить Citrix";
            button.Left = 10;
            button.Top = 20;
            return button;
        }
        
        public Button GetActivateOfficeButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Sysdm.cpl";
            button.Left = 150;
            button.Top = 50;
            return button;
        }
        
        public Button GetActivateWindowsButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Control";
            button.Left = 150;
            button.Top = 80;
            return button;
        }
        
        public Button GetTaskMgrButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "TaskManager";
            button.Left = 150;
            button.Top = 110;
            return button;
        }
        
        public Button GetDeviceButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(110, 20);
            button.Text = "DeviceManager";
            button.Left = 20;
            button.Top = 110;
            return button;
        }
        
        public Button GetUserpasswords2Button()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Userpasswords2";
            button.Left = 150;
            button.Top = 140;
            return button;
        }
        
        public Button GetDeviceInetCplButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(110, 20);
            button.Text = "Inet.Cpl";
            button.Left = 20;
            button.Top = 140;
            return button;
        }
        
        public Button GetDeviceInetCplButton2()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "diskmgmt.msc";
            button.Left = 150;
            button.Top = 170;
            return button;
        }
        
        public Button GetDeviceInetCplButton3()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(110, 20);
            button.Text = "appwiz.cpl";
            button.Left = 20;
            button.Top = 170;
            return button;
        }
        
        public Button GetControlPointButton()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Профиля";
            button.Left = 150;
            button.Top = 20;
            return button;
        }
        
        public Button GetActivateAcronis()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Get Lotus ID";
            button.Left = 20;
            button.Top = 150;
            return button;
        }
        
        public Button GetRezervCopyAcronis()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Сделать резервн. копию";
            button.Left = 150;
            button.Top = 80;
            return button;
        }
        
        public Button GetUtilityWorkFolder()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Utility_Work";
            button.Left = 150;
            button.Top = 120;
            return button;
        }
        
        public Button GetPrintersFolder()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "Printers";
            button.Left = 150;
            button.Top = 160;
            return button;
        }
        
        public Button GetWikiJs()
        {
            Button button = new Button();
            button.Name = "Citrix";
            button.Size = new Size(120, 20);
            button.Text = "WikiJs";
            button.Left = 10;
            button.Top = 160;
            return button;
        }
        
        // кнопки для лотуса
        public Button GetInitLotusButton()
        {
            Button button = new Button();
            button.Name = "Lotus Setup";
            button.Size = new Size(120, 20);
            button.Text = "Install Lotus";
            button.Left = 20;
            button.Top = 50;
            return button;
        }
        
        public Button GetHelperLotusDataButton()
        {
            Button button = new Button();
            button.Name = "lotus";
            button.Size = new Size(120, 20);
            button.Text = "Get Lotus Data";
            button.Left = 20;
            button.Top = 120;
            return button;
        }

        public Button GetSettingLotusButton()
        {
            Button button = new Button();
            button.Name = "Lotus";
            button.Size = new Size(120, 20);
            button.Text = "Setup Lotus";
            button.Left = 20;
            button.Top = 80;
            return button;
        }

        //кнопки админ утилиты
        public Button GetAddAdminButton()
        {
            Button button = new Button();
            button.Name = "Domain";
            button.Size = new Size(120, 20);
            button.Text = "Add Local Admin";
            button.Left = 10;
            button.Top = 20;
            return button;
        }

        public Button GetRemoveAdminButton()
        {
            Button button = new Button();
            button.Name = "Domain";
            button.Size = new Size(120, 20);
            button.Text = "Remove Local Admin";
            button.Left = 10;
            button.Top = 45;
            return button;
        }
        public Button GetAddDomainButton()
        {
            Button button = new Button();
            button.Name = "Domain";
            button.Size = new Size(120, 20);
            button.Text = "Add Domain";
            button.Left = 150;
            button.Top = 20;
            return button;
        }

        public Button GetRemoveDomainButton()
        {
            Button button = new Button();
            button.Name = "Domain";
            button.Size = new Size(120, 20);
            button.Text = "Remove Domain";
            button.Left = 150;
            button.Top = 45;
            return button;
        }
        
        public Button GetChangeNamePcButton()
        {
            Button button = new Button();
            button.Name = "ChangeName";
            button.Size = new Size(120, 20);
            button.Text = "Change NamePC";
            button.Left = 150;
            button.Top = 70;
            return button;
        }
        
        public Button GetAddDomainButtonForce()
        {
            Button button = new Button();
            button.Name = "Domain";
            button.Size = new Size(120, 20);
            button.Text = "Add Domain Force";
            button.Left = 150;
            button.Top = 110;
            return button;
        }
        
        public Button GetRemoveDomainForceButton()
        {
            Button button = new Button();
            button.Name = "Domain";
            button.Size = new Size(120, 20);
            button.Text = "Remove Domain /F";
            button.Left = 150;
            button.Top = 135;
            return button;
        }
        
        public Button GetChangeNamePcButtonForce()
        {
            Button button = new Button();
            button.Name = "ChangeName";
            button.Size = new Size(120, 20);
            button.Text = "Change Name Force";
            button.Left = 150;
            button.Top = 160;
            return button;
        }
        
        public Button GetRebootButton()
        {
            Button button = new Button();
            button.Name = "Reboot";
            button.Size = new Size(120, 20);
            button.Text = "Reboot";
            button.Left = 10;
            button.Top = 140;
            return button;
        }
        
        public Button GetShotDawnButton()
        {
            Button button = new Button();
            button.Name = "Shutdown";
            button.Size = new Size(120, 20);
            button.Text = "Shutdown";
            button.Left = 10;
            button.Top = 170;
            return button;
        }

        public Button GetLogoutButton()
        {
            Button button = new Button();
            button.Name = "Logout";
            button.Size = new Size(120, 20);
            button.Text = "Logout";
            button.Left = 10;
            button.Top = 110;
            return button;
        }

        //кнопки для админ группы
        public Button GetUserButtonInstall()
        {
            Button button = new Button();
            button.Name = "User";
            button.Size = new Size(100, 20);
            button.Text = "Get current user";
            button.Left = 170;
            button.Top = 30;
            return button;
        }
        
        public Button GetPcNameButtonInstall()
        {
            Button button = new Button();
            button.Name = "User";
            button.Size = new Size(100, 20);
            button.Text = "Get Pc Name";
            button.Left = 170;
            button.Top = 120;
            return button;
        }
        
        public Button GetAdminPreset1()
        {
            Button button = new Button();
            button.Name = "User";
            button.Size = new Size(20, 20);
            button.Text = "1";
            button.Left = 170;
            button.Top = 60;
            return button;
        }
        
        public Button GetAdminPreset2()
        {
            Button button = new Button();
            button.Name = "User";
            button.Size = new Size(20, 20);
            button.Text = "2";
            button.Left = 200;
            button.Top = 60;
            return button;
        }
        
        public Button GetAdminPreset3()
        {
            Button button = new Button();
            button.Name = "User";
            button.Size = new Size(20, 20);
            button.Text = "3";
            button.Left = 230;
            button.Top = 60;
            return button;
        }
        
        // инпут поля
        public TextBox GetSalonNameInput()
        {
            TextBox inputSalonName = new TextBox();
            inputSalonName.Size = new Size(100, 20);
            inputSalonName.Text = "";
            inputSalonName.Left = 60;
            inputSalonName.Top = 30;
            return inputSalonName;
        }
        
        public TextBox GetLotusUserName()
        {
            TextBox inputSalonName = new TextBox();
            inputSalonName.Size = new Size(80, 20);
            inputSalonName.Text = "";
            inputSalonName.Left = 150;
            inputSalonName.Top = 50;
            return inputSalonName;
        }
        
        public TextBox GetLotusSameTimeName()
        {
            TextBox inputSalonName = new TextBox();
            inputSalonName.Size = new Size(80, 20);
            inputSalonName.Text = "";
            inputSalonName.Left = 150;
            inputSalonName.Top = 80;
            return inputSalonName;
        }

        public TextBox GetAdminLoginInput()
        {
            TextBox adminLogin = new TextBox();
            adminLogin.Size = new Size(100, 20);
            adminLogin.Text = "y.zyskunov";
            adminLogin.Left = 60;
            adminLogin.Top = 60;
            return adminLogin;
        }

        public TextBox GetAdminPassInput()
        {
            TextBox adminPass = new TextBox();
            adminPass.Size = new Size(100, 20);
            adminPass.PasswordChar = '*';
            adminPass.UseSystemPasswordChar = true;
            adminPass.Left = 60;
            adminPass.Top = 90;
            
            return adminPass;
        }
        
                
        public TextBox GetInputSalonPcName()
        {
            TextBox inputPcName = new TextBox();
            inputPcName.Size = new Size(100, 20);
            inputPcName.Text = "";
            inputPcName.Left = 60;
            inputPcName.Top = 120;
            return inputPcName;
        }

        // лабель дал админ группы
        public Label GetLabelSalonName()
        {
            Label label = new Label();
            label.Text = "User";
            label.Left = 2;
            label.Top = 30;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelLoginName()
        {
            Label label = new Label();
            label.Text = "Login";
            label.Left = 2;
            label.Top = 60;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelPassName()
        {
            Label label = new Label();
            label.Text = "Password";
            label.Left = 2;
            label.Top = 90;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelPCName()
        {
            Label label = new Label();
            label.Text = "Name PC";
            label.Left = 2;
            label.Top = 120;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        // чек бокс
        public CheckBox GetCheckBox1920()
        {
            CheckBox checkBoxLotus = new CheckBox
            {
                Text = "1920х1080",
                Location = new System.Drawing.Point(20, 20),
                Checked = false
            };
            return checkBoxLotus;
        }
        
        public CheckBox GetCheckBoxHCL()
        {
            CheckBox checkBoxLotus = new CheckBox
            {
                Text = "HCL",
                Location = new System.Drawing.Point(130, 20),
                Checked = true
            };
            return checkBoxLotus;
        }

        // helper

        public Button GetCompMGMTButton()
        {
            Button button = new Button();
            button.Name = "Helper";
            button.Size = new Size(110, 20);
            button.Text = "Control PC";
            button.Left = 20;
            button.Top = 50;
            return button;
        }
        
        public Button GetNcpaCplButton()
        {
            Button button = new Button();
            button.Name = "Helper";
            button.Size = new Size(110, 20);
            button.Text = "Ncpa.cpl";
            button.Left = 20;
            button.Top = 80;
            return button;
        }
        
        //инфо панель 
        public Label GetLabelInfoPanelIP()
        {
            Label label = new Label();
            label.Text = "IP:";
            label.Left = 2;
            label.Top = 20;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelInfoPanelIPSet()
        {
            Label label = new Label();
            label.Text = "IP set";
            label.Left = 70;
            label.Top = 20;
            label.Size = new System.Drawing.Size(100, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }
        
        public Label GetLabelInfoPanelPcName()
        {
            Label label = new Label();
            label.Text = "PC Name";
            label.Left = 2;
            label.Top = 40;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelInfoPanelPcMemory()
        {
            Label label = new Label();
            label.Text = "Memory";
            label.Left = 2;
            label.Top = 80;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelInfoPanelFreeSpaceInfo()
        {
            Label label = new Label();
            label.Text = "Disk C:";
            label.Left = 2;
            label.Top = 100;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelMatherBoard()
        {
            Label label = new Label();
            label.Text = "Mather";
            label.Left = 2;
            label.Top = 120;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelProc()
        {
            Label label = new Label();
            label.Text = "Processor";
            label.Left = 2;
            label.Top = 140;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        public Label GetLabelVideo()
        {
            Label label = new Label();
            label.Text = "Video";
            label.Left = 2;
            label.Top = 160;
            label.Size = new System.Drawing.Size(55, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkBlue;
            return label;
        }
        
        // info set panel
        public Label GetInfoPcName()
        {
            Label label = new Label();
            label.Text = "Pc name set";
            label.Left = 70;
            label.Top = 40;
            label.Size = new System.Drawing.Size(200, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }
        
        public Label GetInfoMemory()
        {
            Label label = new Label();
            label.Text = "Memory";
            label.Left = 70;
            label.Top = 80;
            label.Size = new System.Drawing.Size(200, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }
        
        public Label GetInfoSpaceC()
        {
            Label label = new Label();
            label.Text = "Free";
            label.Left = 70;
            label.Top = 100;
            label.Size = new System.Drawing.Size(200, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }
        
       
        public Label GetInfoMatherBoard()
        {
            Label label = new Label();
            label.Text = "mat";
            label.Left = 70;
            label.Top = 120;
            label.Size = new System.Drawing.Size(200, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }
        
        public Label GetInfoProc()
        {
            Label label = new Label();
            label.Text = "prc";
            label.Left = 70;
            label.Top = 140;
            label.Size = new System.Drawing.Size(200, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }
        
        public Label GetInfoVideo()
        {
            Label label = new Label();
            label.Text = "vid";
            label.Left = 70;
            label.Top = 160;
            label.Size = new System.Drawing.Size(200, 20);
            label.TextAlign = ContentAlignment.MiddleLeft; 
            label.ForeColor = System.Drawing.Color.DarkRed;
            return label;
        }

        // Группы
        public GroupBox GetGroupAdminUtility()
        {
            GroupBox groupBox = new GroupBox
            {
                Text = "Admin Utility",
                Location = new Point(10, 10),
                Size = new Size(280, 200)
            };
            return groupBox;
        }
        
        public GroupBox GetGroupAdminParam()
        {
            GroupBox groupBox = new GroupBox
            {
                Text = "Parameters",
                Location = new Point(300, 10),
                Size = new Size(280, 200)
            };
            return groupBox;
        }
        
        public GroupBox GetGroupInfoPanel()
        {
            GroupBox groupBox = new GroupBox
            {
                Text = "Info Panel",
                Location = new Point(600, 10),
                Size = new Size(280, 200)
            };
            return groupBox;
        }
        
        public GroupBox GetGroupLotus()
        {
            GroupBox groupBoxLotus = new GroupBox
            {
                Text = "Lotus Utility",
                Location = new System.Drawing.Point(10, 220),
                Size = new System.Drawing.Size(280, 200)
            };
            return groupBoxLotus;
        }
        
        public GroupBox GetGroupProgramInstall()
        {
            GroupBox groupBox = new GroupBox
            {
                Text = "Program",
                Location = new Point(300, 220),
                Size = new Size(280, 200)
            };
            return groupBox;
        }
        
        public GroupBox GetGroupHelperPanel()
        {
            GroupBox groupBox = new GroupBox
            {
                Text = "Helper",
                Location = new Point(600, 220),
                Size = new Size(280, 200)
            };
            return groupBox;
        }
        
    }
}