﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;
using AutoIt;

namespace WindowsFormsLotus4.Clases
{
    public class Acronis
    {
        public void InstallAcronis()
        {
            InputSimulator simulator = new InputSimulator();

            string pathToExe = @"C:\Program Files (x86)\Acronis\TrueImageHome\TrueImage.exe";
  
            if (System.IO.File.Exists(pathToExe))
            {
                Process.Start(pathToExe);
            }
            else
            {
                MessageBox.Show("Установите Acronis", "Ошибка пути");
                return;
            }
            
            return;

            
        }
    }
}