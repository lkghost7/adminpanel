﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AdminPanel
{
    partial class Form1
    {
        private CheckBox checkBoxLotus;
        private CheckBox checkBoxLotusHCL;
        private GroupBox groupBoxLotus;
        private GroupBox groupBoxAdminUtility;
        private GroupBox groupBoxParametr;
        private TextBox inputSalonName;
        private TextBox inputLotusUserName;
        private TextBox inputLotusSameTimeName;
        private TextBox inputAdminLogin;
        private TextBox inputAdminPass;
        private TextBox inputPcName;
        private Label inputIpSet;
        private Label inputPcSet;
        private Label inputMemorySet;
        private Label inputFreeSpace;
        private Label inputMatherBoard;
        private Label inputProc;
        private Label inputVideo;
        
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        private void InitializeComponent()
        {
            MainWindow();
            InitInputFields();
       
            AddGroups();
        }

        private void AddGroups()
        {
            InitGroupLotus();
            InitGroupAdminUtility();
            InitGroupAdminParameter();
            InitGroupProgramInstall();
            InitGroupInfoPanel();
            InitGroupHelperInstall();
        }

        private void InitInputFields()
        {
            InitSalonNameInput();
            InitLotusUserName();
            InitLotusSameTimeName();
            InitAdminLoginInput();
            InitInputSalonName();
            InitAdminPassInput();
        }
        
        #endregion
    }
}