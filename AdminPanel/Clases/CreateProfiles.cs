﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;
using AutoIt;

namespace WindowsFormsLotus4.Clases
{
    public class CreateProfiles
    {
        public void CreateProfile()
        {
            InputSimulator simulator = new InputSimulator();

            string pathToExe = @"C:\TEMP\FolderProperties\CreateFolders.exe";

            if (System.IO.File.Exists(pathToExe))
            {
                Process.Start(pathToExe);
            }
            else
            {
                MessageBox.Show("Error", "Ошибка пути");
                return;
            }

            Thread.Sleep(2000);
            
            string pathToExe2 = @"C:\TEMP\FolderProperties\FolderProperties_Desk.exe";

            if (System.IO.File.Exists(pathToExe2))
            {
                Process.Start(pathToExe2);
            }
            else
            {
                MessageBox.Show("Error", "Ошибка пути");
                return;
            }
            
            Thread.Sleep(8000);
            
            string pathToExe3 = @"C:\TEMP\FolderProperties\FolderProperties_Doc.exe";

            if (System.IO.File.Exists(pathToExe3))
            {
                Process.Start(pathToExe3);
            }
            else
            {
                MessageBox.Show("Error", "Ошибка пути");
                return;
            }
            
            Thread.Sleep(8000);
            
            string pathToExe4 = @"C:\TEMP\FolderProperties\FolderProperties_Down.exe";

            if (System.IO.File.Exists(pathToExe4))
            {
                Process.Start(pathToExe4);
            }
            else
            {
                MessageBox.Show("Error", "Ошибка пути");
                return;
            }
            
            Thread.Sleep(8000);
            
            string pathToExe5 = @"C:\TEMP\FolderProperties\FolderProperties_Pict.exe";

            if (System.IO.File.Exists(pathToExe5))
            {
                Process.Start(pathToExe5);
            }
            else
            {
                MessageBox.Show("Error", "Ошибка пути");
                return;
            }
            
            Thread.Sleep(8000);

            MessageBox.Show("Профиля прописаны");
        }
    }
}