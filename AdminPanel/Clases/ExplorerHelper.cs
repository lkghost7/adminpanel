﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Management.Automation;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using AutoIt;
using WindowsInput;
using WindowsInput.Native;

namespace WindowsFormsLotus4.Clases
{
    public class ExplorerHelper
    {
                public void ChangeNamePcForce(string pcName2, string login, string pass)
        {
            // Вызов AutoItX для открытия диалога смены имени компьютера
            AutoItX.Run("rundll32.exe Shell32.dll,Control_RunDLL sysdm.cpl","");
            Thread.Sleep(300);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{SPACE}");
            Thread.Sleep(250);
            AutoItX.Send(pcName2);
            Thread.Sleep(70);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(100);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(7000);
            
            AutoItX.Send(login);
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(250);
            AutoItX.Send(pass);
            Thread.Sleep(250);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(250);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(70);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(70);
            AutoItX.Send("{ENTER}");
            // MessageBox.Show($"Имя компьютера успешно изменено на {newComputerName}.");
        }

        public void AddDomainForce(string pcName, string login, string pass)
        {
            AutoItX.Run("rundll32.exe Shell32.dll,Control_RunDLL sysdm.cpl","");
            Thread.Sleep(300);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{SPACE}");
            Thread.Sleep(250);
            AutoItX.Send(pcName);
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(50);
            AutoItX.Send("{UP}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(250);
            AutoItX.Send("nasvyazi");
            Thread.Sleep(100);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(50);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(7000);
            
            AutoItX.Send(login);
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(250);
            AutoItX.Send(pass);
            Thread.Sleep(250);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(250);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(70);
            AutoItX.Send("{ENTER}");
        }
        
        public void RemoveDomainForce(string pcName, string login, string pass)
        {
            AutoItX.Run("rundll32.exe Shell32.dll,Control_RunDLL sysdm.cpl","");
            Thread.Sleep(300);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{SPACE}");
            Thread.Sleep(55);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{DOWN}");
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send(pcName);
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(35);
            AutoItX.Send("{SPACE}");
            Thread.Sleep(35);
            AutoItX.Send("{LEFT}");
            Thread.Sleep(35);
            AutoItX.Send("{SPACE}");
            
            Thread.Sleep(7000);
            AutoItX.Send(login);
            Thread.Sleep(35);
            AutoItX.Send("{TAB}");
            Thread.Sleep(250);
            AutoItX.Send(pass);
            Thread.Sleep(250);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(250);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(50);
            AutoItX.Send("{TAB}");
            Thread.Sleep(70);
            AutoItX.Send("{ENTER}");
        }
        
        
        public void GetLotusPeople(string login, string pass)
        {
            
            InputSimulator simulator = new InputSimulator();
            string pathTo = @"\\10.10.0.253\e$";
            string pathTo2 = @"\\10.10.0.253\e$\Program Files\Lotus\Notes\Data\ids\people";
            
            string windowTitle = "#32770";
            simulator.Keyboard.ModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.VK_R);
            Thread.Sleep(200);
            AutoItX.WinActivate("[TITLE:" + windowTitle + "]");
            Thread.Sleep(200);
            simulator.Keyboard.TextEntry(pathTo);
            Thread.Sleep(800);
            AutoItX.Send("{ENTER}");
     
            Thread.Sleep(3000);
            AutoItX.Send(login);
            Thread.Sleep(400);
            AutoItX.Send("{TAB}");
            Thread.Sleep(400);
            AutoItX.Send(pass);
            Thread.Sleep(400);
            AutoItX.Send("{ENTER}");
            Thread.Sleep(800);
            
            simulator.Keyboard.ModifiedKeyStroke(VirtualKeyCode.MENU, VirtualKeyCode.F4);
            Thread.Sleep(800);
            
            simulator.Keyboard.ModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.VK_R);
            Thread.Sleep(200);
            AutoItX.WinActivate("[TITLE:" + windowTitle + "]");
            Thread.Sleep(200);
            simulator.Keyboard.TextEntry(pathTo2);
            Thread.Sleep(800);
            AutoItX.Send("{ENTER}");
        }
        
        public void LoadUrl(string url)
        {
            try
            {
                Process.Start(url);
                Console.WriteLine("Браузер успешно открыт по адресу: " + url);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при открытии браузера: " + ex.Message);
            }
        }

        public void OpenFolder(string path)
        {
            try
            {
                // Создаем новый процесс для открытия проводника с указанным путем
                Process.Start("explorer.exe", path);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        
        public string GetFreeSpaceOnDrive(string driveLetter)
        {
            string result = String.Empty;
            try
            {
                DriveInfo driveInfo = new DriveInfo(driveLetter);

                if (driveInfo.IsReady)
                {
                    long freeSpace = driveInfo.AvailableFreeSpace;

                    result = $"{freeSpace / (1024 * 1024 * 1024):N2} GB";
                }
                else
                {
                    MessageBox.Show($"Drive {driveLetter} is not ready.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }

            return result;
        }
        
        public string GetMotherboardModel()
        {
            try
            {
                // Используем WMI для запроса информации о материнской плате
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
                ManagementObjectCollection collection = searcher.Get();

                foreach (ManagementObject obj in collection)
                {
                    // Получаем модель материнской платы
                    if (obj["Product"] != null)
                    {
                        return obj["Product"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return "Model Not Found";
        }
        
        public string GetProcessorModel()
        {
            try
            {
                // Используем WMI для запроса информации о процессоре
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Processor");
                ManagementObjectCollection collection = searcher.Get();

                foreach (ManagementObject obj in collection)
                {
                    // Получаем модель процессора
                    if (obj["Name"] != null)
                    {
                        return obj["Name"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return "Model Not Found";
        }
        
        public string GetPhysicalMemory()
        {
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_ComputerSystem");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection collection = searcher.Get();

            string result = String.Empty;
            
            foreach (ManagementObject mo in collection)
            {
                ulong totalPhysicalMemoryKB = (ulong)mo["TotalPhysicalMemory"];
                double totalPhysicalMemoryGB = totalPhysicalMemoryKB / (1024.0 * 1024.0 * 1024.0);
                result = $"{totalPhysicalMemoryGB:F2} GB";
            }
            return result;
        }


        
        public string GetVideoCardInfo()
        {
            try
            {
                // Используем WMI для запроса информации о видеокарте
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
                ManagementObjectCollection collection = searcher.Get();

                foreach (ManagementObject obj in collection)
                {
                    // Получаем информацию о видеокарте
                    if (obj["Caption"] != null)
                    {
                        return obj["Caption"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message}";
            }

            return "Video Card Information Not Found";
        }
    }
}