﻿using System;
using System.Diagnostics;
using System.Management;
using System.Threading;
using System.Windows.Forms;
using AdminPanel.Classes;
using AutoIt;
using WindowsFormsLotus4.Clases;
using WindowsInput;
using WindowsInput.Native;

namespace AdminPanel
{
    public partial class Form1 : Form
    {
        public LotusNote LotusNote { get; set; }
        public CreateProfiles CreateProfiles { get; set; }
        public Acronis Acronis { get; set; }
        public LotusCopyId LotusCopyId { get; set; }
        private PowerShellAdmin _powerShellAdmin;
        private ExplorerHelper _explorerHelper;
        private ButtonsInstall _buttonsInstall;
        public Citrix Citrix { get; set; }
        private int targetX;
        private int targetY;
        public Form1()
        {
            Init();
            InitializeComponent();
        }
        private void Init()
        {
            _powerShellAdmin = new PowerShellAdmin();
            _explorerHelper = new ExplorerHelper();
            _buttonsInstall = new ButtonsInstall();
            LotusNote = new LotusNote();
            CreateProfiles = new CreateProfiles();
            Acronis = new Acronis();
            LotusCopyId = new LotusCopyId();
            Citrix = new Citrix();
            // LotusNote.CreateLotusComplete += OnLotusComplete;
        }
        private void OnLotusComplete() => MessageBox.Show("lotus Поставлен !");

        private void MainWindow()
        {
            components = new System.ComponentModel.Container();
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1000, 600);
            Text = "Admin panel";
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void InitSalonNameInput() => inputSalonName = _buttonsInstall.GetSalonNameInput();
        private void InitLotusUserName() => inputLotusUserName = _buttonsInstall.GetLotusUserName();
        private void InitLotusSameTimeName() => inputLotusSameTimeName = _buttonsInstall.GetLotusSameTimeName();
        private void InitAdminLoginInput() => inputAdminLogin = _buttonsInstall.GetAdminLoginInput();
        private void InitInputSalonName() => inputPcName = _buttonsInstall.GetInputSalonPcName();
        private void InitAdminPassInput() => inputAdminPass = _buttonsInstall.GetAdminPassInput();
        private void PressLotusSetup(object sender, EventArgs e) => LotusNote.LotusSetup(inputLotusSameTimeName.Text, checkBoxLotus);
        // private void PressCitrixInstall(object sender, EventArgs e) => Citrix.CitrixSetup(inputSalonName.Text);
        private void PressCitrixInstall(object sender, EventArgs e) => Citrix.CloseCitrixProcess3(inputSalonName.Text);

        private void PressCreateLotus(object sender, EventArgs e)
        {
            bool isDone = LotusCopyId.LotusFileCopy(inputLotusUserName.Text);

            if (isDone)
            {
                LotusNote.InstallLotus(inputLotusUserName.Text, checkBoxLotusHCL);
            }
        }

        private void PressGetUserName(object sender, EventArgs e)
        {
            
            string scriptPath = @"C:\temp\AdminScripts\GetName.ps1";
            string result = _powerShellAdmin.RunPowerShellScript(scriptPath);
            inputSalonName.Text = result;
        }
        
        private void PressGetUserPcName(object sender, EventArgs e)
        {
            string userPc = Environment.MachineName;
            inputPcName.Text = userPc;
        }
        
        private void PressExplorerLotus253(object sender, EventArgs e)
        {
          _explorerHelper.GetLotusPeople(inputAdminLogin.Text, inputAdminPass.Text);
        }

        private void PressAddAdminButton(object sender, EventArgs e)
        {
            string scriptPath = @"C:\temp\AdminScripts\AddLocalAdmin.ps1";
            _powerShellAdmin.CredentialRunPowerShell(inputAdminPass.Text, inputSalonName.Text, scriptPath, inputAdminLogin.Text);
        }

        private void PressRemoveAdminButton(object sender, EventArgs e)
        {
            string scriptPath = @"C:\temp\AdminScripts\RemoveLocalAdmin.ps1";
            _powerShellAdmin.CredentialRunPowerShell(inputAdminPass.Text, inputSalonName.Text, scriptPath, inputAdminLogin.Text);
        }
        
        private void PressAddDomainButton(object sender, EventArgs e)
        {
            _powerShellAdmin.InDomainPowerShell(inputAdminPass.Text, inputPcName.Text, inputAdminLogin.Text);
        }
        private void PressRemoveDomainButton(object sender, EventArgs e)
        {
            string scriptPath = @"C:\temp\AdminScripts\OutDomain.ps1";
            _powerShellAdmin.CredentialRunPowerShell(inputAdminPass.Text, inputSalonName.Text, scriptPath, inputAdminLogin.Text);
        }
        
        private void PressPreset1(object sender, EventArgs e) => inputAdminLogin.Text = "y.zyskunov";
        private void PressPreset2(object sender, EventArgs e) => inputAdminLogin.Text = "nikita.chachin";
        private void PressPreset3(object sender, EventArgs e) => inputAdminLogin.Text = "kulibin";
        private void PressAddDomainButtonForce(object sender, EventArgs e)
        {
            _explorerHelper.AddDomainForce(inputPcName.Text, inputAdminLogin.Text, inputAdminPass.Text);
        }
        private void PressChangePcButtonForce(object sender, EventArgs e)
        {
            _explorerHelper.ChangeNamePcForce(inputPcName.Text, inputAdminLogin.Text, inputAdminPass.Text);
        }
        private void PressChangePcButton(object sender, EventArgs e)
        {
            string scriptPath = @"C:\temp\AdminScripts\ChangeName.ps1";
            _powerShellAdmin.CredentialRunPowerShell(inputAdminPass.Text, inputPcName.Text, scriptPath, inputAdminLogin.Text);
           
        }
        private void PressLogoutButton(object sender, EventArgs e)
        {
            AutoItX.Run("shutdown.exe /l /f","");
        }
        private void PressRebootButton(object sender, EventArgs e)
        {
            AutoItX.Run("shutdown.exe /r /t 0", "");
        }
        
        private void PressShoutDawnButton(object sender, EventArgs e)
        {
            MessageBox.Show("sdfds");
            AutoItX.Run("shutdown.exe /s /f /t 0", "");
        }
        
        private void CopyProfilesBtn(object sender, EventArgs e)
        {
            CreateProfiles.CreateProfile();
        }
        
        private void RezervAcronisBtn(object sender, EventArgs e)
        {
            Acronis.InstallAcronis();
        }
        
        private void WikiJsBtn(object sender, EventArgs e)
        {
            string url = "http://192.168.5.10:3000";
            _explorerHelper.LoadUrl(url);
        }
        
        private void PrintersBtn(object sender, EventArgs e)
        {
            string folderPath = @"\\192.168.5.238\distrib\_Drivers\Printers";
            _explorerHelper.OpenFolder(folderPath);
        }
        private void UtilityWorkBtn(object sender, EventArgs e)
        {
            string folderPath = @"\\192.168.5.238\distrib\_utilites_for_work";
            _explorerHelper.OpenFolder(folderPath);
        }

        //Группы
        private void InitGroupLotus()
        {
            groupBoxLotus = _buttonsInstall.GetGroupLotus();

            Button buttonSetupLotus = _buttonsInstall.GetSettingLotusButton();
            buttonSetupLotus.Click += PressLotusSetup;
            Button buttonInstallLotus = _buttonsInstall.GetInitLotusButton();
            buttonInstallLotus.Click += PressCreateLotus; 
            checkBoxLotus = _buttonsInstall.GetCheckBox1920();
            checkBoxLotusHCL = _buttonsInstall.GetCheckBoxHCL();
            Button dataLotusButton = _buttonsInstall.GetHelperLotusDataButton();
            dataLotusButton.Click += PressGetDataLotus;
            
                        
            Button buttonLogExplorer = _buttonsInstall.GetActivateAcronis();
            buttonLogExplorer.Click += PressExplorerLotus253;
            
            groupBoxLotus.Controls.Add(checkBoxLotus);
            groupBoxLotus.Controls.Add(checkBoxLotusHCL);
            groupBoxLotus.Controls.Add(buttonSetupLotus);
            groupBoxLotus.Controls.Add(buttonInstallLotus);
            groupBoxLotus.Controls.Add(inputSalonName);
            groupBoxLotus.Controls.Add(inputLotusUserName);
            groupBoxLotus.Controls.Add(inputLotusSameTimeName);
            groupBoxLotus.Controls.Add(dataLotusButton);
            groupBoxLotus.Controls.Add(buttonLogExplorer);

            Controls.Add(groupBoxLotus);
        }

        private void InitGroupAdminUtility()
        {
            groupBoxAdminUtility = _buttonsInstall.GetGroupAdminUtility();

            Button buttonRemoveAdmin = _buttonsInstall.GetRemoveAdminButton();
            buttonRemoveAdmin.Click += PressRemoveAdminButton;
            Button buttonAddAdmin = _buttonsInstall.GetAddAdminButton();
            buttonAddAdmin.Click += PressAddAdminButton;

            Button buttonAddDomain = _buttonsInstall.GetAddDomainButton();
            buttonAddDomain.Click += PressAddDomainButton;
            Button buttonRemoveDomain = _buttonsInstall.GetRemoveDomainButton();
            buttonRemoveDomain.Click += PressRemoveDomainButton;
            
            Button buttonRemoveForceDomain = _buttonsInstall.GetRemoveDomainForceButton();
            buttonRemoveForceDomain.Click += PressRemoveDomainForceButton;
            
            Button buttonAddDomainForce = _buttonsInstall.GetAddDomainButtonForce();
            buttonAddDomainForce.Click += PressAddDomainButtonForce;

            Button buttonReboot = _buttonsInstall.GetRebootButton();
            buttonReboot.Click += PressRebootButton;
            Button buttonLogout = _buttonsInstall.GetLogoutButton();
            buttonLogout.Click += PressLogoutButton;
            Button buttonShotDawn = _buttonsInstall.GetShotDawnButton();
            buttonShotDawn.Click += PressShoutDawnButton;

            Button buttonChangePc = _buttonsInstall.GetChangeNamePcButton();
            buttonChangePc.Click += PressChangePcButton;
            
            Button buttonChangePcForce = _buttonsInstall.GetChangeNamePcButtonForce();
            buttonChangePcForce.Click += PressChangePcButtonForce;
            
            groupBoxAdminUtility.Controls.Add(buttonAddAdmin);
            groupBoxAdminUtility.Controls.Add(buttonRemoveAdmin);
            groupBoxAdminUtility.Controls.Add(buttonAddDomain);
            groupBoxAdminUtility.Controls.Add(buttonRemoveDomain);
            groupBoxAdminUtility.Controls.Add(buttonRemoveForceDomain);
            groupBoxAdminUtility.Controls.Add(buttonReboot);
            groupBoxAdminUtility.Controls.Add(buttonLogout);
            groupBoxAdminUtility.Controls.Add(buttonShotDawn);
            groupBoxAdminUtility.Controls.Add(buttonChangePc);
            groupBoxAdminUtility.Controls.Add(buttonChangePcForce);
            groupBoxAdminUtility.Controls.Add(buttonAddDomainForce);
            
            Controls.Add(groupBoxAdminUtility);
        }
        private void PressRemoveDomainForceButton(object sender, EventArgs e)
        {
            _explorerHelper.RemoveDomainForce("temp","1","1");
        }


        private void InitGroupAdminParameter()
        {
            groupBoxParametr = _buttonsInstall.GetGroupAdminParam();
            Button buttonGetUser = _buttonsInstall.GetUserButtonInstall();
            buttonGetUser.Click += PressGetUserName;
            Button buttonGetPc = _buttonsInstall.GetPcNameButtonInstall();
            buttonGetPc.Click += PressGetUserPcName;
            Button buttonAdminPreset1 = _buttonsInstall.GetAdminPreset1();
            buttonAdminPreset1.Click += PressPreset1;
            Button buttonAdminPreset2 = _buttonsInstall.GetAdminPreset2();
            buttonAdminPreset2.Click += PressPreset2;
            Button buttonAdminPreset3 = _buttonsInstall.GetAdminPreset3();
            buttonAdminPreset3.Click += PressPreset3;

            Label labelSalonName = _buttonsInstall.GetLabelSalonName();
            Label labelLoginName = _buttonsInstall.GetLabelLoginName();
            Label labelPassName = _buttonsInstall.GetLabelPassName();
            Label labelPcName = _buttonsInstall.GetLabelPCName();

            groupBoxParametr.Controls.Add(labelSalonName);
            groupBoxParametr.Controls.Add(labelLoginName);
            groupBoxParametr.Controls.Add(labelPassName);
            groupBoxParametr.Controls.Add(labelPcName);
            
            groupBoxParametr.Controls.Add(buttonGetUser);
            groupBoxParametr.Controls.Add(buttonGetPc);
            groupBoxParametr.Controls.Add(buttonAdminPreset1);
            groupBoxParametr.Controls.Add(buttonAdminPreset2);
            groupBoxParametr.Controls.Add(buttonAdminPreset3);
            
            groupBoxParametr.Controls.Add(inputSalonName);
            groupBoxParametr.Controls.Add(inputPcName);
            groupBoxParametr.Controls.Add(inputAdminLogin);
            groupBoxParametr.Controls.Add(inputAdminPass);
            
            Controls.Add(groupBoxParametr);
        }

        private void InitGroupProgramInstall()
        {
            groupBoxParametr = _buttonsInstall.GetGroupProgramInstall();
            Button buttonCitrix = _buttonsInstall.GetCitrixInstallButton();
            buttonCitrix.Click += PressCitrixInstall;
            
            Button buttonControlPoint = _buttonsInstall.GetControlPointButton();
            buttonControlPoint.Click += CopyProfilesBtn;
            Button buttonRezervCopyAcronis = _buttonsInstall.GetRezervCopyAcronis();
            buttonRezervCopyAcronis.Click += RezervAcronisBtn;
            
            Button utilityWorkFolder = _buttonsInstall.GetUtilityWorkFolder();
            utilityWorkFolder.Click += UtilityWorkBtn;
            
            Button printerFolder = _buttonsInstall.GetPrintersFolder();
            printerFolder.Click += PrintersBtn;
            
            Button wikiJs = _buttonsInstall.GetWikiJs();
            wikiJs.Click += WikiJsBtn;
            
            groupBoxParametr.Controls.Add(buttonCitrix);
            groupBoxParametr.Controls.Add(buttonControlPoint);
            groupBoxParametr.Controls.Add(buttonRezervCopyAcronis);
            groupBoxParametr.Controls.Add(utilityWorkFolder);
            groupBoxParametr.Controls.Add(printerFolder);
            groupBoxParametr.Controls.Add(wikiJs);
            
            Controls.Add(groupBoxParametr);
        }

        private void InitGroupHelperInstall()
        {
            GroupBox helperPanel = _buttonsInstall.GetGroupHelperPanel();
            
            Button compMgmtButton = _buttonsInstall.GetCompMGMTButton();
            compMgmtButton.Click += MgmtCompBtn;
            
            Button ncpaCplBtn = _buttonsInstall.GetNcpaCplButton();
            ncpaCplBtn.Click += NcpaCPLButton;
            Button systemCplBtn = _buttonsInstall.GetActivateOfficeButton();
            systemCplBtn.Click += SystemCPLButton;
            Button controlSysBtn = _buttonsInstall.GetActivateWindowsButton();
            controlSysBtn.Click += ControlSysButton;
            Button taskMgrButton = _buttonsInstall.GetTaskMgrButton();
            taskMgrButton.Click += TaskmgrBtn;
            Button deviceButton = _buttonsInstall.GetDeviceButton();
            deviceButton.Click += DeviceMgrBtn;
            Button userpasswords2Button = _buttonsInstall.GetUserpasswords2Button();
            userpasswords2Button.Click += userpasswords2Btn;
            Button deviceInetCplButton = _buttonsInstall.GetDeviceInetCplButton();
            deviceInetCplButton.Click += inetCplBtn;
            
            Button deviceInetCplButton2 = _buttonsInstall.GetDeviceInetCplButton2();
            deviceInetCplButton2.Click += inetCplBtn2;
            
            Button deviceInetCplButton3 = _buttonsInstall.GetDeviceInetCplButton3();
            deviceInetCplButton3.Click += inetCplBtn3;

            helperPanel.Controls.Add(compMgmtButton);
            helperPanel.Controls.Add(ncpaCplBtn);
            helperPanel.Controls.Add(systemCplBtn);
            helperPanel.Controls.Add(controlSysBtn);
            helperPanel.Controls.Add(taskMgrButton);
            helperPanel.Controls.Add(deviceButton);
            helperPanel.Controls.Add(userpasswords2Button);
            helperPanel.Controls.Add(deviceInetCplButton);
            helperPanel.Controls.Add(deviceInetCplButton2);
            helperPanel.Controls.Add(deviceInetCplButton3);
            
            Controls.Add(helperPanel);
        }
        private void inetCplBtn(object sender, EventArgs e)
        {
            Process.Start("inetcpl.cpl");
        }
        
        private void inetCplBtn2(object sender, EventArgs e)
        {
            Process.Start("diskmgmt.msc");
        }
        
        private void inetCplBtn3(object sender, EventArgs e)
        {
            Process.Start("appwiz.cpl");
        }
        private void userpasswords2Btn(object sender, EventArgs e)
        {
            AutoItX.Run("control userpasswords2", "");
        }
        private void DeviceMgrBtn(object sender, EventArgs e)
        {
            Process.Start("devmgmt.msc");
        }
        private void TaskmgrBtn(object sender, EventArgs e)
        {
            Process.Start("taskmgr");
        }
        private void MgmtCompBtn(object sender, EventArgs e)
        {
            Process.Start("compmgmt.msc");
        }
        private void ControlSysButton(object sender, EventArgs e)
        {
            AutoItX.Run("control", "");
        }
        private void SystemCPLButton(object sender, EventArgs e)
        {
            AutoItX.Run("rundll32.exe Shell32.dll,Control_RunDLL sysdm.cpl","");
        }
        
        private void NcpaCPLButton(object sender, EventArgs e)
        {
            AutoItX.Run("rundll32.exe Shell32.dll,Control_RunDLL ncpa.cpl","");
        }

        private void InitGroupInfoPanel()
        {
            GroupBox infoPanel = _buttonsInstall.GetGroupInfoPanel();
            Label ip = _buttonsInstall.GetLabelInfoPanelIP();
            inputIpSet = _buttonsInstall.GetLabelInfoPanelIPSet();
            
            Label pcName = _buttonsInstall.GetLabelInfoPanelPcName();
            inputPcSet = _buttonsInstall.GetInfoPcName();
            
            Label pcMemory = _buttonsInstall.GetLabelInfoPanelPcMemory();
            inputMemorySet = _buttonsInstall.GetInfoMemory();
            
            Label freeSpace = _buttonsInstall.GetLabelInfoPanelFreeSpaceInfo();
            inputFreeSpace = _buttonsInstall.GetInfoSpaceC();
            
            Label matherBoard = _buttonsInstall.GetLabelMatherBoard();
            inputMatherBoard = _buttonsInstall.GetInfoMatherBoard();
            
            Label proc = _buttonsInstall.GetLabelProc();
            inputProc = _buttonsInstall.GetInfoProc();
            
            Label video = _buttonsInstall.GetLabelVideo();
            inputVideo = _buttonsInstall.GetInfoVideo();
            
            infoPanel.Controls.Add(ip);
            infoPanel.Controls.Add(inputIpSet);
            
            infoPanel.Controls.Add(pcName);
            infoPanel.Controls.Add(inputPcSet);
            
            infoPanel.Controls.Add(pcMemory);
            infoPanel.Controls.Add(inputMemorySet);
            
            infoPanel.Controls.Add(freeSpace);
            infoPanel.Controls.Add(inputFreeSpace);
            
            infoPanel.Controls.Add(matherBoard);
            infoPanel.Controls.Add(inputMatherBoard);
            
            infoPanel.Controls.Add(proc);
            infoPanel.Controls.Add(inputProc);
            
            infoPanel.Controls.Add(video);
            infoPanel.Controls.Add(inputVideo);
            
            Controls.Add(infoPanel);
            SetInfoParam();
        }
        private void PressGetDataLotus(object sender, EventArgs e)
        {
            string currentUser = Environment.UserName;
            string localAppDataPath = $@"C:\Users\{currentUser}\AppData\Local\HCL\Notes";
            _explorerHelper.OpenFolder(localAppDataPath);
            // string proc = _explorerHelper.GetProcessorModel();
            // string model = _explorerHelper.GetMotherboardModel();
            // MessageBox.Show(proc);
        }
        
        private void SetInfoParam()
        {
            _powerShellAdmin.EnablePohForce();
            
            string[] ip = _powerShellAdmin.GetAllIPv4Addresses();
            inputIpSet.Text = ip[0];

            string userPc = Environment.MachineName;
            inputPcSet.Text = userPc;

            string memory = _explorerHelper.GetPhysicalMemory();
            inputMemorySet.Text = memory;

            inputFreeSpace.Text = _explorerHelper.GetFreeSpaceOnDrive("C:\\");
            inputMatherBoard.Text = _explorerHelper.GetMotherboardModel();
            string procInfo = _explorerHelper.GetProcessorModel();
            inputProc.Text = procInfo.Length > 35 ? procInfo.Substring(0, 35) : procInfo;

            string video = _explorerHelper.GetVideoCardInfo();
            inputVideo.Text = video;
        }

    }
}