﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;
using AutoIt;

namespace AdminPanel.Classes
{
    public class LotusNote
    {
        public Action CreateLotusComplete;
        public void InstallLotus(string target, CheckBox chekHCL)
        {
            InputSimulator simulator = new InputSimulator();

            string resultPath = String.Empty;
            string pathToExe = @"C:\Program Files (x86)\IBM\Notes\notes.exe";
            string pathToExeHCL = @"C:\Program Files (x86)\HCL\Notes\notes.exe";

            if (chekHCL.Checked)
            {
                resultPath = pathToExeHCL;
            }
            else
            {
                resultPath = pathToExe;
            }

            if (System.IO.File.Exists(resultPath))
            {
                Process.Start(resultPath);
            }
            else
            {
                MessageBox.Show("Установите лотус", "Ошибка пути");
                return;
            }

            Thread.Sleep(2000);
            simulator.Keyboard.KeyPress(VirtualKeyCode.SPACE); // запустили приложение нажали далее
            Thread.Sleep(100);

            string command1 = target;
            simulator.Keyboard.TextEntry(command1);
            Thread.Sleep(100); // ввели имея пользователя нажали далее

            simulator.Keyboard.KeyPress(VirtualKeyCode.TAB);
            Thread.Sleep(50);
            string command2 = "lotus/nasvyazi";
            simulator.Keyboard.TextEntry(command2);
            Thread.Sleep(100); // ввели именя лотуса

            simulator.Keyboard.KeyPress(VirtualKeyCode.TAB);
            simulator.Keyboard.KeyPress(VirtualKeyCode.TAB);
            Thread.Sleep(100); //2 таба вниз после ввода пользователя и домена

            simulator.Keyboard.KeyPress(VirtualKeyCode.SPACE);
            Thread.Sleep(100); // нажали далее после ввода пользоватлей 

            string command4 = "C:\\TEMP\\" + target + ".id";
            simulator.Keyboard.TextEntry(command4);
            Thread.Sleep(1000);

            simulator.Keyboard.KeyPress(VirtualKeyCode.TAB);
            simulator.Keyboard.KeyPress(VirtualKeyCode.TAB);

            Thread.Sleep(200); //нажали 2 раза таб 
            simulator.Keyboard.KeyPress(VirtualKeyCode.SPACE);
            Thread.Sleep(100);
            simulator.Keyboard.KeyPress(VirtualKeyCode.SPACE); 

            Thread.Sleep(250);
            
            string command3 = "q123456";
            simulator.Keyboard.TextEntry(command3);
            Thread.Sleep(150);
            simulator.Keyboard.KeyPress(VirtualKeyCode.RETURN); // ввел пароль 123456 нажал энетр

            Thread.Sleep(3200); // нажал далее когда появилось окно
            simulator.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            
            // Thread.Sleep(35000);
            // LotusSetup();
        }

        public void LotusSetup(string nameLotus, CheckBox chekHCL)
        {
            string windowClass = "SWT_Window0";
            AutoItX.WinActivate("[CLASS:" + windowClass + "]");
            Thread.Sleep(600);
            AutoItX.WinSetState("[CLASS:" + windowClass + "]", "", AutoItX.SW_MAXIMIZE);
            Thread.Sleep(650);
            AutoItX.Send("^9");
            Thread.Sleep(400);

            int targetX = 45;
            int targetY = 55;
            AutoItX.MouseClick("left", targetX, targetY);

            Thread.Sleep(400);

            targetX = 90;
            targetY = 247;
            AutoItX.MouseClick("left", targetX, targetY);

            Thread.Sleep(400);

            targetX = 323;
            targetY = 248; 
            AutoItX.MouseClick("right", targetX, targetY);

            Thread.Sleep(500);
            targetX = 489;
            targetY = 459;
            AutoItX.MouseClick("left", targetX, targetY);

            Thread.Sleep(800);
            AutoItX.Send("{ENTER}");

            Thread.Sleep(800);
            AutoItX.Send("{ENTER}");
            
            Thread.Sleep(300);
            // return;
            if (chekHCL.Checked)
            {
                targetX = 1830;
                targetY = 200;
            }
            else
            {
                targetX = 1254;
                targetY = 200;
            }
            
            Thread.Sleep(500);
            
            // targetX = 1254;
            // targetY = 200;
            
            
            AutoItX.MouseClick("left", targetX, targetY);
            Thread.Sleep(1000);
            AutoItX.Send("sametime");

            Thread.Sleep(80);
            AutoItX.Send("{TAB}");
            AutoItX.Send(nameLotus);

            Thread.Sleep(80);
            AutoItX.Send("{TAB}");
            AutoItX.Send("q123456");

            Thread.Sleep(80);
            AutoItX.Send("{TAB}");
            Thread.Sleep(80);
            AutoItX.Send("{SPACE}");

            Thread.Sleep(80);
            AutoItX.Send("{TAB}");
            Thread.Sleep(80);
            AutoItX.Send("{SPACE}");

            Thread.Sleep(40);
            AutoItX.Send("{TAB}");
            Thread.Sleep(40);
            AutoItX.Send("{TAB}");
            Thread.Sleep(40);
            AutoItX.Send("{TAB}");
            Thread.Sleep(40);
            AutoItX.Send("{TAB}");
            Thread.Sleep(80);
            AutoItX.Send("{ENTER}");

            Thread.Sleep(200);
            AutoItX.Send("{TAB}");
            Thread.Sleep(200);
            AutoItX.Send("{ENTER}");
            
            // CreateLotusComplete.Invoke();
        }
    }
}