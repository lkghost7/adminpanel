﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace AdminPanel.Classes
{
    public class LotusCopyId
    {
        public bool LotusFileCopy(string input)
        {
            string remoteFolderPath = @"\\192.168.5.238\distrib\_utilites_for_work\[LK]\link";
            string fileNameToFind = input + ".id".ToLower();
            string localFolderPath = @"C:\temp\";
            bool result = false;

            try
            {
                DirectoryInfo remoteDirectory = new DirectoryInfo(remoteFolderPath);
                if (!remoteDirectory.Exists)
                {
                    MessageBox.Show($"Папка {remoteFolderPath} не доступна." + "  решите проблему, нажмите ок");
                    return false;
                }

                FileInfo[] matchingFiles = remoteDirectory.GetFiles(fileNameToFind, SearchOption.TopDirectoryOnly);

                if (matchingFiles.Length == 0)
                {
                    MessageBox.Show($"Файл {fileNameToFind} не найден в папке {remoteFolderPath}." + "  решите проблему, нажмите ок");
                    return false;
                }

                string remoteFilePath = matchingFiles[0].FullName;
                string uri = remoteFilePath.Replace('\\', '/');

                using (WebClient webClient = new WebClient())
                {
                    string localFilePath = Path.Combine(localFolderPath, fileNameToFind);

                    webClient.DownloadFile(uri, localFilePath);

                    MessageBox.Show($"Файл успешно скопирован в {localFilePath}." + "  нажмить ок для продолжения");
                    result = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Произошла ошибка: {ex.Message}" + "  нажмите ок для продолжения");
                result = false;
            }
            return result;
        }
    }
}